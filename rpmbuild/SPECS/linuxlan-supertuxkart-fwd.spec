Summary: Firewalld service for supertuxkart for our LinuxLANs
Name: linuxlan-supertuxkart-fwd
Version: 1.0.1
Release: 1%{?dist}
License: GPLvv
#Group: System Environment/Libraries
Source: %{name}-%{version}.tar.xz
URL: https://gitlab.com/dl1chb/linuxlan
#BuildRequires:
Provides: linuxlan-supertuxkart-fwd = %{version}-%{release}
Requires: firewalld-filesystem
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
This is a portable abstraction library for DVD decryption which is used by
the VideoLAN project, a full MPEG2 client/server solution.  You will need
to install this package in order to have encrypted DVD playback with the
VideoLAN client and the Xine navigation plugin.

%global debug_package %{nil}

%prep

%setup -q

%build

%install
rm -rf $RPM_BUILD_ROOT
install -D -m 644 supertuxkart.xml %{buildroot}%{_prefix}/lib/firewalld/services/supertuxkart.xml

%clean
rm -rf $RPM_BUILD_ROOT

#%post
#%firewalld_reload

%files
%dir %{_prefix}/lib/firewalld
%dir %{_prefix}/lib/firewalld/services
%{_prefix}/lib/firewalld/services/supertuxkart.xml

%changelog

* Thu Feb 03 2022 Christopher Beck (beckus at beckus dot eu) 1.0.0-1
- Created 1.0.0

* Fri Feb 04 2022 Christopher Beck (beckus at beckus dot eu) 1.0.1-1
- changed port to udp
