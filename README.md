# linuxlan

SPECs and stuff for LinuxLANs using OpenSource Games only

Packages can be found at [COPR](https://copr.fedorainfracloud.org/coprs/beckus/LinuxLAN/)

## Getting started

Install RPMS using copr:
* dnf copr enable beckus/LinuxLAN

# Included Things
## RPM Packages (SPEC-files)
* firewalld config for supertuxkart (linuxlan-supertuxkart-fwd)
